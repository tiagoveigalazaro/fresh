#!/usr/bin/ruby

OWNER = "Tiago"

ROOT_DIR = File.expand_path(File.dirname(__FILE__))

require_relative '_includes/tty'

def system(*args)
  abort "Failed during: #{args.shell_s}" unless Kernel.system(*args)
end

def sudo(*args)
  args.unshift("-A") unless ENV["SUDO_ASKPASS"].nil?
  ohai "/usr/bin/sudo", *args
  system "/usr/bin/sudo", *args
end

ohai "Configuring #{OWNER}'s MacOS..."

modules = Dir.glob("*").select {|entry| File.directory? entry}

modules.sort.each do |item|
  next if item.start_with?("_", ".")
  #props = eval(File.open("#{item}/_properties") {|f| f.read })
  
  system("/usr/bin/ruby", "#{ROOT_DIR}/#{item}/install.rb")

end
