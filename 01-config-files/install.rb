#!/usr/bin/ruby

require_relative '../_includes/tty'
require 'fileutils'

ROOT_DIR = File.expand_path(File.dirname(__FILE__))
DEFAULT_TARGET = ENV['HOME']

ohai "Copying configuration files..."

fileConfigs = eval(File.open("#{ROOT_DIR}/_properties") {|f| f.read })[:items]

files = Dir.glob("#{ROOT_DIR}/[!_]*", File::FNM_DOTMATCH).tap { |a| a.shift(2) }.select {|entry| File.file? entry}.reject do |file|
  file.end_with? ".rb"
end

files.each do |file|
  config = fileConfigs.detect {|c| c[:filename] == File.basename(file)}

  if config.nil? then
    target = DEFAULT_TARGET
  else
    target = config[:target]
  end

  FileUtils.cp(file, target)
end

puts "#{Tty.green}==> Done, copied #{files.length} files!#{Tty.reset}"
