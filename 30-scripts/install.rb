#!/usr/bin/ruby

require_relative '../_includes/tty'
require 'fileutils'

ROOT_DIR = File.expand_path(File.dirname(__FILE__))
DEFAULT_TARGET = ENV['HOME']

props = eval(File.open("#{ROOT_DIR}/_properties") {|f| f.read })

ohai props[:description]

# Loads all scripts
scripts = Dir.glob("#{ROOT_DIR}/*.sh")

scripts.sort.each do |script|
  system("chmod", "+x", script)
  puts "Executing #{script}"
  system("#{script}")
end

puts "#{Tty.green}==> Done, executed #{scripts.length} files!#{Tty.reset}"
