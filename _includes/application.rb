#!/usr/bin/ruby

APPLICATION_DIR = "/Applications"

module Application

  module_function  

  def installed?(app)
    if app.end_with?(".app") then
      fullname = "#{APPLICATION_DIR}/#{app}"
      return File.exist?(fullname) && File.directory?(fullname)
    else
      return command?(app)
    end
  end

  # Cross-platform way of finding an executable in the $PATH.
  # Credits: https://stackoverflow.com/questions/2108727/which-in-ruby-checking-if-program-exists-in-path-from-ruby?answertab=votes#tab-top
  def command?(cmd)
    exts = ENV['PATHEXT'] ? ENV['PATHEXT'].split(';') : ['']
    ENV['PATH'].split(File::PATH_SEPARATOR).each do |path|
    exts.each { |ext|
      exe = File.join(path, "#{cmd}#{ext}")
      return File.executable?(exe) && !File.directory?(exe)
    }
    end
    return false
  end

end
