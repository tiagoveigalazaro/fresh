#!/usr/bin/ruby

ROOT_DIR = File.expand_path(File.dirname(__FILE__))

require_relative '../_includes/tty'
require_relative '../_includes/application'

props = eval(File.open("#{ROOT_DIR}/_properties") {|f| f.read })

ohai props[:description]

applications = props[:items]

applications.each do |app|

  name = app[:name]
  test = app[:command]
  install = app[:install]

  print "Checking for #{name}..."

  if Application.installed? test then

    puts "#{Tty.green}already installed, ignoring...#{Tty.reset}"

  else
    puts "#{Tty.bold} Not found, installing...#{Tty.reset}"

    # Install the application by executing the supplied command.
    install.each do |command|
      system(command)
    end

    done
  end
end

done
