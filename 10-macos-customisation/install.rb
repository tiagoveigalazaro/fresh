#!/usr/bin/ruby

require_relative '../_includes/tty'

ROOT_DIR = File.expand_path(File.dirname(__FILE__))

ohai "Performing MacOs Customisations..."

configs = eval(File.open("#{ROOT_DIR}/_properties") {|f| f.read })[:items]

results = []

configs.each do |config|

  res = system(config)
  results.push($?.exitstatus)
end

### Because we push every result into the arraym the sizes should match
if results.length != configs.length then
  warn "Not all commands were applied!"

### A successful command returns 0, so the sum should be in any case still 0
elsif results.inject(:+) != 0 then
  warn "Failed to execute some commands!"
else
  done
end
